# Demo Test Git
Project for getting to know Git with support software.

## Source
[![alt text](https://static.vecteezy.com/system/resources/thumbnails/000/609/893/small/dog-05.jpg)](https://jaktestowac.pl/)

## Technologies:
- Git 2.20
- PyCharm 2018.3.5
- Python 3.7
- Windows 10 Pro
  
## External Platform
[GitLab](https://gitlab.com) - repository hosting